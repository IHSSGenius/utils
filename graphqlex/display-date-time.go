package graphqlex

import (
	"time"

	"bitbucket.org/IHSSGenius/utils/logger"
	"github.com/graphql-go/graphql"
	"github.com/graphql-go/graphql/language/ast"
)

//const Rfc3339Format = "2006-01-02T15:04:05Z+07:00"
const Rfc3339Format = "2006-01-02T15:04:05.999999Z07:00"
const displayFormatWithTimezone = "2006-01-02 15:04:05 -0700"

const DisplayFormat = "2006-01-02 15:04:05"

var DisplayDateTime = graphql.NewScalar(graphql.ScalarConfig{
	Name: "DisplayDateTime",
	Description: "The `DateTime` scalar type represents a DateTime." +
		" The DateTime is serialized in the format yyyy-MM-dd hh:mm:ss quoted string," +
		" also SA time zone is assumed",
	Serialize:  serializeDateTime,
	ParseValue: unserializeDateTime,
	ParseLiteral: func(valueAST ast.Value) interface{} {
		switch valueAST := valueAST.(type) {
		case *ast.StringValue:
			return unserializeDateTime(valueAST.Value)
		}
		return nil
	},
})

//Takes time and converts to string (output format 2006-01-02 15:04:05)
//Also takes strings in the format "2006-01-02T15:04:05Z+07:00" from internal communication (nats) and output to strings of the format 2006-01-02 15:04:05
func serializeDateTime(value interface{}) interface{} {
	switch value := value.(type) {
	case time.Time:
		if location, err := time.LoadLocation("Local"); err != nil {
			logger.Errorf("Could not get location: %v", err)
			return value.Format(DisplayFormat)
		} else {
			return value.In(location).Format(DisplayFormat)
		}
	case *time.Time:
		if value == nil {
			logger.Debug("Nil pointer")
			return nil
		}
		return serializeDateTime(*value)
	case string:
		t, err := time.Parse(Rfc3339Format, value)
		if err != nil {
			if value == "" {
				logger.Errorf("Serialize DateTime failed as the input string is empty")
			} else {
				logger.Errorf("Serialize DateTime failed as the input string was in the wrong format (not 2006-01-02T15:04:05.999999Z07:00): %+v", value)
			}
			return nil
		}
		return serializeDateTime(t)
	case *string:
		if value == nil {
			return nil
		}
		return serializeDateTime(*value)
	default:
		return nil
	}
}

//Converts string to time
//This is called when converting from the incoming graphql and placing the values in the args map[string]interface
//It needs to take in 2006-01-02 15:04:05 from the graphql
func unserializeDateTime(value interface{}) interface{} {
	switch value := value.(type) {
	case []byte:
		t, err := time.Parse(displayFormatWithTimezone, string(value)+" +0200") //This shifts the time to the SA time zone
		if err != nil {
			return nil
		}
		return t
	case string:
		return unserializeDateTime([]byte(value))
	case *string:
		if value == nil {
			return nil
		}
		return unserializeDateTime([]byte(*value))
	case time.Time:
		return value
	default:
		return nil
	}
}
