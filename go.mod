module bitbucket.org/IHSSGenius/utils

go 1.14

require (
	github.com/fsnotify/fsnotify v1.4.9
	github.com/graphql-go/graphql v0.7.9
	github.com/mitchellh/mapstructure v1.3.3
	github.com/nats-io/jwt v1.2.0 // indirect
	github.com/nats-io/nats-server/v2 v2.1.9 // indirect
	github.com/nats-io/nats.go v1.10.0
	github.com/op/go-logging v0.0.0-20160315200505-970db520ece7
	github.com/spf13/viper v1.7.1
	golang.org/x/crypto v0.0.0-20201016220609-9e8e0b390897 // indirect
	golang.org/x/sys v0.0.0-20201106081118-db71ae66460a // indirect
	google.golang.org/protobuf v1.25.0 // indirect
)
